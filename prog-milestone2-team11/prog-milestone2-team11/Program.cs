﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone2_team11
{
    class Program
    {
        static void Main(string[] args)
        {
            var option = 0;

            do
            {
                WelcomeText(ref option);

 // Task 1 starts here (Olena Spektor)          
   
                if (option == 1)
                {
                    Console.Clear();

                    DateTime todayDate = DateTime.Today;      
                    DateTime userDateBirth = new DateTime();    
                    int subOption1 = 0;

                    Submenu1(ref subOption1);

                    do
                    {
                        switch (subOption1)
                        {
                            case 1:
                                ExecuteOption1Suboption1(todayDate, userDateBirth, ref subOption1);
                                break;

                            case 2:
                                ExecuteOption1Suboption2(todayDate, ref subOption1);
                                break;

                            case 3:
                                Console.Clear();
                                break;
                        }

                    } while (subOption1 ==1 || subOption1 == 2);

                }

// Task 2 starts here (Surinder)

                if (option == 2)
                {
                    Console.Clear();
                    string[] paperCode = { " ", " ", " ", " " };
                    double[] mark = { 0.0, 0.0, 0.0, 0.0 };
                    string grade = "";
                    int studID = 0;
                    int level = 0;
                    var avg = 0;
                    int i = 0;
                    int subOption2 = 0;
                    PaperAndCodeInput(paperCode, mark, ref level, ref studID);

                    Console.Clear();
                    
                    SubMenu2(ref subOption2);

                    do
                    {
                        if (subOption2 == 1)
                        {
                            Console.Clear();
                            Console.WriteLine($"You are Level {level} \nYour student ID: {studID}\n");
                            Console.WriteLine("\nPaper Code \tMark \tGrade");

                            if (level == 5)
                            {
                                for (i = 0; i <= 3; i++)
                                {
                                    grade = GradeCalculator(mark, grade, i);
                                    Console.WriteLine($"{paperCode[i]} \t\t{mark[i]} \t{grade}");
                                }
                            }
                            else
                            {
                                for (i = 0; i <= 2; i++)
                                {
                                    grade = GradeCalculator(mark, grade, i);
                                    Console.WriteLine($"{paperCode[i]} \t\t{mark[i]} \t{grade}");
                                }
                            }
                            Console.WriteLine("\nGo back - press <b> or <B>");
                            subOption2 = char.Parse(Console.ReadLine());

                            if (subOption2 == 'b' || subOption2 == 'B')
                            {
                                Console.Clear();
                                SubMenu2(ref subOption2);
                            }
                        }

                        if (subOption2 == 2)
                        {
                            Console.Clear();

                            if (level == 5)
                            {
                                AverageMarks(mark, level, avg);
                            }
                            else
                            {
                                AverageMarks(mark, level, avg);
                            }
                            Console.WriteLine("\nGo back - press <b> or <B>");
                            subOption2 = char.Parse(Console.ReadLine());

                            if (subOption2 == 'b' || subOption2 == 'B')
                            {
                                Console.Clear();
                                SubMenu2(ref subOption2);
                            }
                        }

                        if (subOption2 == 3)
                        {
                            Console.Clear();

                            HigherMark(paperCode, mark, grade, ref level, i);

                            if (mark[i] < 85)
                            {
                                Console.WriteLine("You don't have marks with A+ grade.");
                            }
                        
                            Console.WriteLine("\nGo back - press <b> or <B>");
                            subOption2 = char.Parse(Console.ReadLine());

                            if (subOption2 == 'b' || subOption2 == 'B')
                            {
                                Console.Clear();
                                SubMenu2(ref subOption2);
                            }
                        }
                    } while (subOption2 ==1 || subOption2 == 2 || subOption2 == 3);
                }


// Task 3 starts here (Gurtej)
                if (option == 3)
                {
                    Console.Clear();                  
                }


// Task 4 starts here (Bikramjit)
                if (option == 4)
                {
                    Console.Clear();

                    var favouriteFood = new Dictionary<string, int>();
                    var favouriteFood2 = new Dictionary<string, int>();
                    var favouriteFood3 = new Dictionary<string, int>();
                    var favouriteFood4 = new Dictionary<string, int>();
                    var favouriteFood5 = new Dictionary<string, int>();

                    Console.WriteLine("type your food:");
                    string favfood = Console.ReadLine();
                    int rankNumber = int.Parse(Console.ReadLine());
                    favouriteFood.Add(favfood, rankNumber);

                    Console.WriteLine($"{favfood}, {rankNumber}");
                    Console.WriteLine("type your food:");
                    favfood = Console.ReadLine();
                    rankNumber = int.Parse(Console.ReadLine());
                    favouriteFood2.Add(favfood, rankNumber);

                    Console.WriteLine($"{favfood}, {rankNumber}");

                    Console.WriteLine("type your food:");
                    favfood = Console.ReadLine();
                    rankNumber = int.Parse(Console.ReadLine());
                    favouriteFood3.Add(favfood, rankNumber);

                    Console.WriteLine($"{favfood}, {rankNumber}");

                    Console.WriteLine("type your food:");
                    favfood = Console.ReadLine();
                    rankNumber = int.Parse(Console.ReadLine());
                    favouriteFood4.Add(favfood, rankNumber);

                    Console.WriteLine($"{favfood}, {rankNumber}");

                    Console.WriteLine("type your food:");
                    favfood = Console.ReadLine();
                    rankNumber = int.Parse(Console.ReadLine());
                    favouriteFood5.Add(favfood, rankNumber);

                    Console.WriteLine($"{favfood}, {rankNumber}");

                }

                if (option == 5)
                {
                    GoodBye();
                    break;
                }

            } while (true);

        }
// ******************************************************************************************************************************************************
// Olena's methods begin here

            static void WelcomeText(ref int option)
        {
            Console.Clear();
            Console.WriteLine("******************************************");
            Console.WriteLine("*                                        *");
            Console.WriteLine("*        WELCOME TO TEAM 11 APP          *");
            Console.WriteLine("*                                        *");
            Console.WriteLine("*  (Please choose what you want to do    *");
            Console.WriteLine("*         by typing in a number)         *");
            Console.WriteLine("*                                        *");
            Console.WriteLine("*                                        *");
            Console.WriteLine("*      1. Date Calculator                *");
            Console.WriteLine("*      2. Calculate Grade Average        *");
            Console.WriteLine("*      3. Generate a Random Number       *");
            Console.WriteLine("*      4. Rate Favourite Food            *");
            Console.WriteLine("*      5. Exit                           *");
            Console.WriteLine("*                                        *");
            Console.WriteLine("******************************************");
            Console.WriteLine();

            Console.Write("Please choose an option here:  ");
            option = int.Parse(Console.ReadLine());

            if (option <= 0 || option >= 6)
            {
                do
                {
                    Console.WriteLine("");
                    Console.WriteLine("Please, choose the correct option.");
                    option = int.Parse(Console.ReadLine());
                } while (option <= 0 || option >= 6);
            }
        }

        static void ExecuteOption1Suboption1(DateTime todayDate, DateTime userDateBirth, ref int subOption1)
        {
            do
            {
                Console.Clear();
                Console.WriteLine($"Today is {todayDate.ToShortDateString()}.\n");

                Console.WriteLine(UserDaysOld(userDateBirth, todayDate));

                Console.ReadLine();

                Console.WriteLine("Go back - press <b> or <B>");
                subOption1 = char.Parse(Console.ReadLine());

                if (subOption1 == 'b' || subOption1 == 'B')
                {
                    Console.Clear();
                    Submenu1(ref subOption1);
                }

            } while (subOption1 == 1);
        }

        static void ExecuteOption1Suboption2(DateTime todayDate, ref int subOption1)
        {
            int numberOfYears = 0;    
            int totalNumberOfDays = 0;
            int leapYears = 0;
            int currentYear = 2016;
            const int loop = 4; 
            int daysInNormalYear = 0;    
           
            do
            {
                Console.Clear();
                Console.WriteLine($"The current date is {todayDate.ToShortDateString()}.\n");

                Console.WriteLine(NumberDaysInYear(numberOfYears, totalNumberOfDays, leapYears, currentYear, loop, daysInNormalYear));

                Console.ReadLine();

                Console.WriteLine("Go back - press <b> or <B>");
                subOption1 = char.Parse(Console.ReadLine());

                if (subOption1 == 'b' || subOption1 == 'B')
                {
                    Console.Clear();
                    Submenu1(ref subOption1);
                }

            } while (subOption1 == 2);
        }

        static string Submenu1(ref int subOption1)
        {
            Console.WriteLine("*********************************************");
            Console.WriteLine("*       YOU ARE IN DATE CALCULATOR          *");
            Console.WriteLine("*                                           *");
            Console.WriteLine("*   1. How many days old are you?           *");
            Console.WriteLine("*   2. How many days in a number of years?  *");
            Console.WriteLine("*   3. Go to Main menu                      *");
            Console.WriteLine("*                                           *");
            Console.WriteLine("*                                           *");
            Console.WriteLine("*********************************************\n\n");
            Console.Write("Please choose a number and press <enter>: ");
            subOption1 = int.Parse(Console.ReadLine());
            
            return $"{subOption1}";
        } 

        static string UserDaysOld(DateTime userDateBirth, DateTime todayDate)
        {
            Console.WriteLine("What's your date of birth? (dd/mm/yyyy)\n");
            userDateBirth = DateTime.Parse(Console.ReadLine());

            TimeSpan span = todayDate.Subtract(userDateBirth);

            return $"\nYou are {span.TotalDays} days old now.\nPress <enter> to continue...";
        }


        static string NumberDaysInYear(int numberOfYears, int totalNumberOfDays, int leapYears, int currentYear, int loop, int daysInNormalYear)
        {
            Console.Write("How many years do you want to calculate: ");
            numberOfYears = int.Parse(Console.ReadLine());

            leapYears = ((currentYear % loop) - numberOfYears) / -loop;

            daysInNormalYear = numberOfYears * 365;
            totalNumberOfDays = daysInNormalYear + leapYears;

            return $"\nThere are {totalNumberOfDays} days in {numberOfYears} years time.\nPress <enter> to continue...";
        }

        static void GoodBye()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("****************************");
            Console.WriteLine("**                        **");
            Console.WriteLine("**    See you later!      **");
            Console.WriteLine("**                        **");
            Console.WriteLine("**    Press <ENTER>       **");
            Console.WriteLine("**                        **");
            Console.WriteLine("****************************");
            Console.ReadLine();
        }

// Olena's methods finish here
// ******************************************************************************************************************************************************





// ******************************************************************************************************************************************************
// Surinder's methods begin here

        static void PaperAndCodeInput(string[] paperCode, double[] mark, ref int level, ref int studID)
        {
            Console.WriteLine("Are you Level 5 or 6? ");
            level = int.Parse(Console.ReadLine());

            Console.Write("\nYour student ID: ");
            studID = int.Parse(Console.ReadLine());
            if (level == 5)
            {
                for (int i = 0; i <= 3; i++)
                {
                    Console.Write("\nPaper code: ");
                    paperCode[i] = Console.ReadLine();

                    Console.Write("Mark:\t");
                    mark[i] = double.Parse(Console.ReadLine());
                }
            }
            else
            {
                for (int i = 0; i <= 2; i++)
                {
                    Console.Write("\nPaper code: ");
                    paperCode[i] = Console.ReadLine();

                    Console.Write("Mark:\t");
                    mark[i] = double.Parse(Console.ReadLine());
                }
            }
        }

        static string GradeCalculator(double[] mark, string grade, int i)
        {
            if (mark[i] >= 90)
            {
                grade = "A+";
            }
            else if (mark[i] >= 85)
            {
                grade = "A";
            }
            else if (mark[i] >= 80)
            {
                grade = "A-";
            }
            else if (mark[i] >= 75)
            {
                grade = "B+";
            }
            else if (mark[i] >= 70)
            {
                grade = "B";
            }
            else if (mark[i] >= 65)
            {
                grade = "B-";
            }
            else if (mark[i] >= 60)
            {
                grade = "C+";
            }
            else if (mark[i] >= 55)
            {
                grade = "C";
            }
            else if (mark[i] >= 50)
            {
                grade = "C-";
            }
            else if (mark[i] >= 40)
            {
                grade = "D";
            }
            else if (mark[i] <= 39)
            {
                grade = "E";
            }
            return grade;
        }

        static void AverageMarks(double[] mark, int level, double avg)
        {
            if (level == 5)
            {
                avg = (mark[0] + mark[1] + mark[2] + mark[3]) / 4;
            }
            else
            {
                avg = (mark[0] + mark[1] + mark[2]) / 3;
            }

            Console.WriteLine($"Your average marks = {avg}");
        }

        static void HigherMark(string[] paperCode, double[] mark, string grade, ref int level, int i)
        {
            grade = GradeCalculator(mark, grade, i);
           
            for (i = 0; i <= 3; i++)
            {
                if (mark[i] >= 85)
                {
                    Console.WriteLine($"You got {grade} with {mark[i]} for {paperCode[i]}");
                }
            } 
        }

        static void SubMenu2(ref int subOption2)
        {
            Console.WriteLine("*************************************************");
            Console.WriteLine("*  What do you want to do with your data?       *");
            Console.WriteLine("*                                               *");
            Console.WriteLine("*  1. To see the summery of input info          *");
            Console.WriteLine("*  2. Calculate the average of all marks        *");
            Console.WriteLine("*  3. Only papers with <A+> score               *");
            Console.WriteLine("*  4. Go to Main menu                           *");
            Console.WriteLine("*                                               *");
            Console.WriteLine("*************************************************");
            Console.Write("\nPlease type in an option: ");

            subOption2 = int.Parse(Console.ReadLine());
        }


        // Surinder's methods finish here
        // ******************************************************************************************************************************************************





        // ******************************************************************************************************************************************************
        // Gurtej's methods begin here

        // Gurtej's methods finish here
        // ******************************************************************************************************************************************************





        // ******************************************************************************************************************************************************
        // Bikramjit's methods begin here



        // Bikramjit's methods finish here
        // ******************************************************************************************************************************************************

    }


}
